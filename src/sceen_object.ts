import {MyShape,ShapeData} from "./loader";
//mport {Scene,Mesh} from "babylonjs";
/// <reference path="../node_modules/babylonjs/dist/preview release/babylon.d.ts" />

export class ShapeObject {
    public static  SELECT_COLOR:number[] = [255,0,0];
    private _data:  ShapeData[] ; 
    private _object: BABYLON.Mesh;
    private _material : BABYLON.StandardMaterial;
    private _color: number[];

    private _trail:BABYLON.LinesMesh;
    private _time_vector: number[];
    private _trail_points:BABYLON.Vector3[] = [];

    private _updatable: boolean;
    private _moving: boolean;
    private _name: string;
    private _scene: BABYLON.Scene;
  
  public constructor(sceen: BABYLON.Scene, shape: MyShape, name: string) {
      this._updatable = shape.shape_changed == 1;
      this._moving = shape.moving_object == 1;
      this._name = name;
      this._scene = sceen;
      this._color = shape.data[0].base.color;
      switch(shape.type){
      case "box":
        this._object = BABYLON.Mesh.CreateBox(name ,1,sceen,this._updatable); 
        break;       
      case "sphere":
        this._object = BABYLON.Mesh.CreateSphere(name,16,1,sceen,this._updatable);
        break;
      case "cylinder":
       this._object = BABYLON.Mesh.CreateCylinder(name,1,1,1,16,1,sceen,this._updatable);
        break;
      case "cone":
       this._object = BABYLON.Mesh.CreateCylinder(name,1,0,1,16,1,sceen,this._updatable);
        break;
      default:
        console.log("Unkown type: " + shape.type);
    }
    if (this._object !== undefined) {
        //var matrix = BABYLON.Matrix.Translation(0,0.5,0);
        var matrix = BABYLON.Matrix.Translation(0,0.5,0).multiply(BABYLON.Matrix.RotationAxis(new BABYLON.Vector3(0,0,1),-3.1415926/2)) ;
        this._object.bakeTransformIntoVertices(matrix);
        // color speculation and transperancy
        this._material =  new BABYLON.StandardMaterial("texture_"+name, sceen);
        this.set_color(this._color, shape.data[0].base.s_coef);
        // assign the material
        this._object.material = this._material;  
              
    };
    this.scale(shape.data[0].size);
    this.rotate(shape.data[0].base.x_dir,shape.data[0].base.y_dir)
    this.set_position(shape.data[0].base.r);
    this._create_trail(shape.data);
  }

 /** Objec has been selected highlit it in the sceen and diplay a trail if it is avaliable. **/
  public select():void {
   // this._material.specularColor = new BABYLON.Color3(1, 1, 1);
  //  this._material.specularPower = this._material.specularPower *2;
    this.set_color(ShapeObject.SELECT_COLOR);
    if (this._moving) {
        this._hide_show_object(this._trail,true);
    }
  }

  public deselect() {
     this.set_color(this._color);
     if (this._moving) {
        this._hide_show_object(this._trail,false);
     }
  }

  public get_babylon_obj():BABYLON.AbstractMesh {
    return this._object;
  }

  /** Creating a line mesh to display trail if needed */
  private _create_trail(data:ShapeData[]){
    this._time_vector=[];
    if (this._moving) {
        for(let d of data){
            this._time_vector.push(d.time);
            this._trail_points.push(new BABYLON.Vector3( d.base.r[0],d.base.r[1],d.base.r[2]));
        }
        this._trail =  BABYLON.Mesh.CreateLines(this._name+"_trail",this._trail_points,this._scene);
        //Hide the trail during the initialisation
        this._hide_show_object(this._trail,false);
      }
  }

  /** Hiding and showing the object/or its trail**/
  private _hide_show_object(obj:BABYLON.AbstractMesh,show:boolean) {
      if (obj !== undefined) {
        let index = this._scene.meshes.indexOf(obj);
        if (show && index << 0) {
            this._scene.meshes.push(obj);
        } else {
            this._scene.meshes.splice(index, 1);
        }
      }
  }


  private set_color(colour:number[], s_coef?:number):void{
     if (this._object !== undefined) {
        this._material.diffuseColor = new BABYLON.Color3(colour[0]/255, colour[1]/255, colour[2]/255);
        this._material.backFaceCulling = true;
        if (s_coef !== undefined) {
           // this._material.specularPower = 100*s_coef;
        }

     }

  }

  private scale(size:number[]) :void {
     if (this._object !== undefined) {
        this._object.scaling.x = size[0]; // width  (y)
        this._object.scaling.y = size[1]; // length (x)
        this._object.scaling.z = size[2]; // height (z)
     }
  }

  private set_position(r:number[]):void {
     if (this._object !== undefined) {
        this._object.position.x = r[0];
        this._object.position.y = r[1];
        this._object.position.z = r[2];
     }
  }

  private rotate(x_axis:number[], y_axis:number[]):void {
     if (this._object !== undefined) {
        var x_ = new BABYLON.Vector3(x_axis[0], x_axis[1], x_axis[2]);
        var y_ = new BABYLON.Vector3(y_axis[0], y_axis[1], y_axis[2]);
        var rot = BABYLON.Vector3.RotationFromAxis(x_, y_,  BABYLON.Vector3.Cross(x_,y_));
        this._object.rotation = rot;
     }
  }

  //public controll methods
  
}