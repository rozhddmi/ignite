

/// <reference path="../node_modules/babylonjs/dist/preview release/babylon.d.ts" />


import {get_objects,MyShape,parse_shape} from "./loader";
import {ShapeObject} from "./sceen_object";
import {MyCamera} from "./int_camera"

import "./canvas_clicker";

export  class GameInt {
  private static _instance:GameInt;
  private _canvas: HTMLCanvasElement;
  private _engine: BABYLON.Engine;
  private _scene: BABYLON.Scene;
  private _camera: MyCamera;
  private _light: BABYLON.Light;
  // objects map
  private _objects: { [key:string]:ShapeObject; };
  // selected object
  private _selected_object: ShapeObject;
  // current time of the simulation
  private _curent_time: number;



  // private holder to store canvas click time to prevent long click -> select
  private _mouse_press_time =0;
  private constructor(canvasElement : string) {
    // Create canvas and engine
    this._canvas = <HTMLCanvasElement>  document.getElementById(canvasElement);
    this._engine = new BABYLON.Engine(this._canvas, true);
    this._objects = {};
    this._curent_time = 0;
    GameInt._instance = this;
  }

  public static create(canvas:string):GameInt {

    if(GameInt._instance) {
      return GameInt._instance;
    } else {
      return new GameInt(canvas);
    }
  }

  public  createScene() : void {
      // create a basic BJS Scene object
      this._scene = new BABYLON.Scene(this._engine);

      // create a FreeCamera, and set its position to (x:0, y:5, z:-10)
      this._camera = new MyCamera(this._scene);
      this._camera.attachControl(this._canvas);
      // create a basic light, aiming 0,1,0 - meaning, to the sky
      this._light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0,-100,0), this._scene);
      this._light.intensity = 0.5;
      let light = new BABYLON.HemisphericLight('light2', new BABYLON.Vector3(0,0,-100), this._scene);
      light.intensity = 0.5;
      let light2 = new BABYLON.HemisphericLight('light2', new BABYLON.Vector3(0,0,100), this._scene);
      light2.intensity = 0.5;

  }




  public parse_json(data:string):void{
    var obj:MyShape =  parse_shape(data);
    this._objects[obj.ext_obj_id] = new ShapeObject(this._scene,obj,""+obj.ext_obj_id);
  }

  public get_selected():ShapeObject {
    return this._selected_object;
  }

  private select(name: string):boolean {
    //selecting new
    if (name in this._objects && this._selected_object !== this._objects[name]) {
      console.log("Selectring: "+name);
      if (this._selected_object !== undefined) {
         this._selected_object.deselect();
      }
      this._selected_object = this._objects[name];
      this._selected_object.select();
      return true;
    }
    return false;
  }

  public canvasSelectObject(x:number,y:number) : ShapeObject {
    // We try to pick an object
    let cvn_clk = this._canvas.relMouseCoords(x,y);
    var pickResult:BABYLON.PickingInfo = this._scene.pick(cvn_clk.clientX, cvn_clk.clientY);
    if (pickResult.hit) {
      var name:string = pickResult.pickedMesh.name;
      if (this.select(name)){
        return this._selected_object;
      }
    } else {
       if (this._selected_object !== undefined) {
         this._selected_object.deselect();
         this._selected_object = undefined;
      }
    }
    return undefined;
  }

  public  animate() : void {   

    this._engine.runRenderLoop(() => {
        this._scene.render();
    });

    // the canvas/window resize event handler
    this._canvas.addEventListener('resize', () => {
      this._engine.resize();
    });  


    //preventing hold and click..
    this._canvas.addEventListener("mousedown",  (evt:any)=> {
      this._mouse_press_time =evt.timeStamp;
    });

    this._canvas.addEventListener("mouseup",  (evt:any)=> {
      if(evt.timeStamp- this._mouse_press_time < 200) {
        this.canvasSelectObject(evt.pageX,evt.pageY);
      }
    });

    this._canvas.removeEventListener("oncontextmenu");
    /***
    window.addEventListener("contextmenu",(e) => {
      e.preventDefault();
    });   
    */
  }

 
}


// funclion adding 

