import * as React from "react";
import * as ReactDOM from "react-dom";
import { App } from "./components/App";

import {BrowserRouter,Route,Switch,} from "react-router-dom"



ReactDOM.render(
    <BrowserRouter>
        <App canvas_name="render_canvas" main_path="C:/Users/dr14/Documents/Dymola/vis" />
    </BrowserRouter >,
    document.getElementById("example")
);