export {get_objects};



function get_objects(path: string):MyShape[] {
    return [];
}

export  function parse_shape (str: string):MyShape {
    let obj: MyShape = JSON.parse(str);
    return obj;
}


export  interface MyShape {
    ext_obj_id:number,
    isUR:number,
    type:string,
    data:ShapeData[],
    moving_object:number,
    shape_changed:number

}

export interface Base {
    extra:number,
    s_coef:number,
    color:number[],
    x_dir:number[],
    y_dir:number[],
    r:number[]
}

export interface ShapeData{
    time:number,
    base:Base,
    size:number[]
}

