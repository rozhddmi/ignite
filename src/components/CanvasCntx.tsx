import * as React from "react";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import {GameInt} from "../game"
import {ShapeObject} from "../sceen_object"


enum Listener {
    Render,
    Object,
}

enum RenderCommands {
  ResetView,
  FitView,
  ResetObjects
}

enum ObjectCommands {
  Hide,
  Follow,
  Transparent
}


interface MenuRenderCP {
  command:RenderCommands;
}

interface MenuObjectCP {
  command:ObjectCommands;
}

export interface CanvasCntxProps {get_game:{():GameInt}, id:string}

// 'HelloProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class CanvasCntx extends React.Component<CanvasCntxProps, undefined> {
    private handleRenderCallback:Function; // wrapper with bind
    private handleObjectCallback:Function; // wrapper with bind

    private elementSelected:Function; // wrapper with bind 
    private onMenuOpen: {(event:any):void};

    public constructor(){
      super();
      this.handleRenderCallback = this._handleRenderCallback.bind(this);
      this.handleObjectCallback = this._handleObjectCallback.bind(this);

      this.elementSelected = this._elementSelected.bind(this);
      this.onMenuOpen =this._onOpen.bind(this);
    }

    private _onOpen(event: any):void{
      let g:GameInt = this.props.get_game();
      if (g){
        g.canvasSelectObject(event.detail.position.x,event.detail.position.y);
        this.forceUpdate();
      }
    }

    /***Call backs related to renderer */
    private _handleRenderCallback(e:Event,data:MenuRenderCP):void {
      let g:GameInt = this.props.get_game();
      if (g === undefined){
        return;
      }
      console.log(g);
      switch (data.command){
        case RenderCommands.FitView:
        break;

        case RenderCommands.ResetObjects:
        break;

        case RenderCommands.ResetView:
        break;
      }
    }
    /***Callbacks related to objects */
    private _handleObjectCallback(e:Event,data:MenuObjectCP):void {
       let g:GameInt = this.props.get_game();
       if (g === undefined || g.get_selected() === undefined){
        return;
       }
      let obj:ShapeObject = g.get_selected();
      console.log(obj);
      switch (data.command){
        case ObjectCommands.Follow:
        break;

        case ObjectCommands.Hide:
        break;

        case  ObjectCommands.Transparent:
        break;
      }
    }


    private _elementSelected():boolean {
      if (this.props.get_game() === undefined){
        return false;
      }
      return (this.props.get_game().get_selected() !== undefined);
    }



    render() {
        return <ContextMenu id={this.props.id} onShow={this.onMenuOpen}>
        { 
          (this.elementSelected()
            ? <div>
                <MenuItem  data={{command:ObjectCommands.Hide}}  onClick={this.handleObjectCallback}> {'Hide element'}</MenuItem>
                <MenuItem  data={{command:ObjectCommands.Follow}}  onClick={this.handleObjectCallback}> {'Follow element'}</MenuItem>
                <MenuItem  data={{command:ObjectCommands.Transparent}}  onClick={this.handleObjectCallback}> {'See through'}</MenuItem>
                <MenuItem divider />
              </div>
              : null
          )
         }
            <MenuItem data={{command:RenderCommands.ResetView}} onClick={this.handleRenderCallback}>{'Reset View'}</MenuItem>
            <MenuItem data={{command:RenderCommands.ResetObjects}} onClick={this.handleRenderCallback}>{'Reset Objects'}</MenuItem>
            <MenuItem data={{command:RenderCommands.FitView}} onClick={this.handleRenderCallback}>{'Fit All'}</MenuItem>
      </ContextMenu>
    }
}