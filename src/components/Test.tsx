import * as React from "react";


export interface TestProps { }

// 'HelloProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class Test extends React.Component<any, undefined> {
    
    render() {
        return  <h1>Hello from Test !</h1>;    
    }
}