import * as React from "react";
import {GameInt} from "../game";
import axios, { AxiosRequestConfig, AxiosPromise } from 'axios';
import {CanvasCntx} from "./CanvasCntx";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import {ShapeObject} from "../sceen_object";


export interface AppProps { canvas_name: string, main_path: string}

// 'HelloProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class App extends React.Component<AppProps, undefined> {
    private game:GameInt;
    private static _fetched_data:string[];
    private  _get_url:string = "http://localhost:5000/get_data"
   
    //Context menu internal components
    private _contex_menu_id:string = "render_canvas_contex";
    private _get_game:{():GameInt};


    public constructor(){
        super();
        this._get_game = (function():GameInt{return this.game}).bind(this);
    }
    
    /** Getting each separate file */
    private _fetchFile(file:string):void{
        let data_request = {path: this.props.main_path,
                            request: "get_file",
                            file:file}
                
        axios.post(this._get_url,data_request).then(
            res=>{
                if (res.status === 200){ 
                    this.game.parse_json(res.data);
                }
            }
        )

    }

    /**getting list of files */
    private _fetchList():void{
        let data_request = {
            path: this.props.main_path,
            request: "get_list"
        }        
        axios.post(this._get_url,data_request).then(
            res=>{
                if (res.status === 200){ 
                    res.data.forEach((value:string, i:number) => {
                        this._fetchFile(value);
                    });
                }
            }
        )
    }


    componentDidMount(){
        console.log("Canvas about_to_create");
        this.game = GameInt.create("renderCanvas");
        this.game.createScene();
        this.game.animate();

        this._fetchList();
        //this.game.createScene();
    }

    render() {
        return <div className="App-header">
                    <h1>Here will be all controll elements {this.props.canvas_name} !</h1>
                    <ContextMenuTrigger id={this._contex_menu_id} holdToDisplay={-1} >
                                    <canvas id="renderCanvas"></canvas>

                    </ContextMenuTrigger> 
                    <CanvasCntx get_game={this._get_game} 
                                id = {this._contex_menu_id}
                    />                
                </div>
                
    }
}