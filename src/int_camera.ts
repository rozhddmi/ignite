/// <reference path="../node_modules/babylonjs/dist/preview release/babylon.d.ts" />
import {ShapeObject} from "./sceen_object"

export class MyCamera { 
    private _initial_position:BABYLON.Vector3;
    private _camera:BABYLON.ArcRotateCamera;
    private _zero_mesh:BABYLON.AbstractMesh;
    private _scene:BABYLON.Scene;
    private _initial_radius:number;


    public constructor(scene:BABYLON.Scene, pos?:BABYLON.Vector3 ,dist?:number){
        this._scene =scene;
        this._initial_position =(pos)? pos:new BABYLON.Vector3(0, 0, 0);
        this._initial_radius = (dist)?dist:10;
        this._zero_mesh =BABYLON.Mesh.CreateSphere('zero_intern',1,0,this._scene);
        this._zero_mesh.position = this._initial_position;

        let ind = this._scene.meshes.indexOf(this._zero_mesh);
        this._scene.meshes.splice(ind,1);
        //this._camera = new BABYLON.ArcFollowCamera('arc_camera',0,0, this._initial_radius , this._zero_mesh,this._scene);
        this._camera = new BABYLON.ArcRotateCamera('arc_camera',0,0, this._initial_radius ,this._initial_position,this._scene);
        this._camera.minZ = 0;
        this._camera.maxZ = 500;
    }
    
    public followObject(obj:ShapeObject):void{
        this._camera.target = obj.get_babylon_obj().position;
    }

    public _reset():void{
        this._camera.target = this._initial_position;
        this._camera.radius = this._initial_radius;

    }

    public attachControl(canvas:any) {
        this._camera.setPosition(new BABYLON.Vector3(0, 0, 10));
       // this._camera.upVector = new BABYLON.Vector3(0,0,1);
        this._camera.panningInertia = 0.1;
        this._camera.rotation.x = 3.14/2
        this._camera.allowUpsideDown = true;
        this._camera.noRotationConstraint=true;
        this._camera.panningSensibility = 200;
        this._camera.inertia = 0.1;
        this._camera.angularSensibilityX = 200;
                this._camera.angularSensibilityY = 200;
        this._camera.attachControl(canvas,true);


    }




}