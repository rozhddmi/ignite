export interface canvasClick{
  clientX:number;
  clientY:number;
}

declare global {
    interface HTMLCanvasElement {
        relMouseCoords(x:number,y:number) :canvasClick;
    }
}

HTMLCanvasElement.prototype.relMouseCoords = function(this:HTMLCanvasElement,x:number,y:number):canvasClick{
    var totalOffsetX = 0;
    var totalOffsetY = 0;
    var canvasX = 0;
    var canvasY = 0;
    var currentElement:any = this;

    do{
        totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
        totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
    }
    while(currentElement = currentElement.offsetParent)

    canvasX = x - totalOffsetX;
    canvasY = y - totalOffsetY;

    return {clientX:canvasX, clientY:canvasY}
}