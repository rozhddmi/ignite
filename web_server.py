from flask import Flask, request, url_for, render_template,send_from_directory,json
import os
print os.getcwd()

app = Flask(__name__, template_folder=r".", static_folder= r'C:/Users/dr14/Personal/TypeScript/react_project/')

@app.route('/')
def hello_world():
    return render_template('index.html')  # "Hello World!"

@app.route('/<path:filename>')
def serve_static(filename):
    print "serving static"
    #root_dir = os.getcwd()
    dir  = os.path.join( os.getcwd(),os.path.dirname(filename))
    print os.path.join(dir),os.path.basename(filename)
    return send_from_directory(os.path.join(dir),os.path.basename(filename))



@app.route('/get_data', methods=['GET','POST'])
def request_data():
    parsed = json.loads(request.data)
    if parsed['request'] == "get_list":
        path = parsed['path']
        files = [os.path.join(path,os.path.basename(f)) for f in os.listdir(parsed['path']) if f.endswith('json')]
        response = app.response_class(
                     response=json.dumps(files),
                     status=200,
                     mimetype='application/json')
    elif parsed['request'] == "get_file":
        path = parsed['file']
        if os.path.isfile(path):
            with open(path) as f:
                jf = f.read()
            response = app.response_class(
                         response=json.dumps(jf),
                         status=200,
                         mimetype='application/json')
        else:
            response = app.response_class(
                         response=None,
                         status=100,)
    else:
        response = app.response_class(
            response=None,
            status=100,)
    return response

def get_files_with_ext(path,ext):
    files = [os.path.basename(f) for f in os.listdir(path) if f.endswith(ext)]

if __name__ == '__main__':
    app.run(debug=True,)

